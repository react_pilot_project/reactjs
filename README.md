We require Node.js v10.x and we areinstalling it using PPA from [Digital Ocean Guide](https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-18-04).

## Available Scripts

The set up script is already downloaded as `nodesource_setup.sh`

run the script
### `sudo bash nodesource_setup.sh`
Install nodejs
### `sudo apt install nodejs`
Check version
### `nodejs -v`
After confirming the version is `v10.14.0`, get started on the project.